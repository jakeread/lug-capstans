# Log LC-SMF

### LC-SMF Hookup Guide

B: SER3 PA20 -
C: SER2 PA08 - ok, spindle
D: SER4 PB12 - ok, X
E: SER0 PA04 - ok, Y
F: SER5 PB00 - ok, Z

## 2020 02 25

Unfortunately I can still kind of see some difference in motion on left and right sides of the y axis, and now am kind of wondering if really the wire rope drive is worth the time. Certainly, the way forward to PIP is to take deep breaths and spend more time with this flawed piece of hardware, to figure it out.

This counts at the control level as well, where I can rebuild everything using these controllers - including the bus. To do that, I should get bus-drop circuits out sooner rather than later.

## 2020 02 24

OK, long live LC-SMF, I'm back here doing re-assembly w/ these crimped loops, so measurements: *and* the note that one must first burn the section one wants to strip, then clip, then strip. For 2" of stripped length, I get a loop w/ ~ a diameter of 10.5mm, a center ~ 15mm from the clip (and the bottom of the ferrule). Given CAD, it should be possible to measure an ideal loop-to-loop length, and just make these very carefully: we will see how true this is in practice.

So that's more like:

| Stripped Length | Resting Loop Inner Diameter | Loop Inner Tip | Loop Center | Ferrule Length |
| --- | --- | --- | --- | --- |
| 45mm | 7mm | 22mm | 17mm | 8mm |

OK, got one of these axis buttoned up w/ the properly cinched wire rope. I had to really over-tension this here because I undershot the length (which, shooting right is a tough pickle), and as I kind of suspected here's the idler:

![idler-flex](2020-02-24_lc-bend.jpg)

Really walking out of its place, but here's the nice twang:

![twang](2020-02-24_lc-twang.mp4)

This sounds about right. Next, I'm going to turn this on and see about crashing it into a hard stop, and see what breaks. Wish the LC luck. If I'm up to it, I'll doodle out the rest of the night re-assembling this wire path... yikes.

This seems much nicer, I think that all I see now is motor stall ... I should, besides, have some motor code that stops during stall events. I would rather spend my time doing that, and adding a new jogging (by increment-in-2d-click-plane) hunk, than in toiling on this halfway-fix for the LC.

![crunch](2020-02-24_lc-crunch.mp4)

I guess I'm on some restringing, then. Suffer now and I can pinch myself some time for tomorrow morning / OSAP work. Here's the loop:

![cinch](2020-02-24_lc-adhoc-cinch.jpg)

## 2020 02 14

![k](2020-02-14_wire-loop.jpg)

## 2020 02 11

Assy notes (below) reasonable span 01-28 -> 02-07, for reference. Takes a while to build a machine, unless you've done (that instance) before. I've spent ~ 3 days here with this at solidworks, trying to get the thing to sing and dance.

The biggest take away is that the capstans *do* creep over time, and tensioning is really important. I have had all axis loosen, especially when / after they are hard-stopped accidentally. This means two things: (1) first that cable terminations should be done *properly* i.e. with crimp-on fittings, I've ordered some of these, to do this. Loops afford us 100% of the rope strength, and single-stops afford 40%, so I'll go for the loops first, as I almost everywhere have enough space for these. (2) second, that I should have smarter motor controllers that stop when stalled. again, this makes me want simpler firmware wrappers and protocols, so that editing firmware codes isn't encumbered by the gosh-dang dataflow systems.

Also! for setup, I should have separate (cut) lengths between each run. Because I have CAD, I can calculate ideal lengths for each of these loops before I install them, and make separate hold-down devices for each 'take-off' on the line. I imagine this will greatly make-easier the multi-constrained tensioning getdown I had to deal with earlier.

Coming out of this I think I am learning lots about the cable drives, and am psyched, if I can get the crimp terminations to work. At some points I think 'why the heck am I not just putting belts in this thing' but then I remember that that would be boring. I am hotly anticipating the emergence of PIP from this work. Assembly notes below are valuable wisdom.

## 2020 01 28

Through to assembly now.

### ASSY NOTES

- *another* and kritical step (bump 2 top) is that set-screw-lazily-into-plastic for the pinion / drive grooved idler is *not good enough* at all, comes loose. some options:
 - revert to pulley-to-reduction, drive via reduction (I like this) now we can use purchase parts pinions, ok
 - for DD, use some other motor, I am imagining the NTM motors, w/ their back-of-can M2 holes.
- so far I get along with M3FHCSx14mm only, save:
 - 16mm for rollers on all idlers (bearing thru)
- the bearings in the vee-idler-thing: if the top is flush, pressing the bearing is would be ez-er
- no clearance for heat-set insert tool on motor mounts for y, z
- open up clearance on the pgd / beam, at the idlers.
- closing the beam does a lot of work... all visible flex comes from the rear parallelogram
- the PIP should really dial in on the kinematics, running torsion bearings just on one side of the beam: there is really no guarantee that these will be parallel, with the layup. one set of bearings for the beam-x-support (that right-side double-pinch in this version)
- on the x beam, some thru-holes to easily mount the motor in the core, if the core is already in the machine...
- close out the mouth-guard on the pip, for hundred (and log video of this deflection)
- hole in motor mounts, for when shcs is in pulley / pinion, and pushing motor into housing, or, spec set screw for these
- centered-ness of these idlers is not cool enough, I'm going to fix this now
- ok, fixed that on-center problem, but could do better in another rev, certainly. now I am having a rubbing issue... if the grooves are too close, the cross-ways will rub, of course! I can fix this with a bigger spread between grooves... I'll also throw the same update into the other two sides of this thing, then get those printing
- the y-dof needs to be careful, indeed! and you need to figure how to lay these straight-edges down without having them lean over... and minimizing glue near those bearings would be cool. really needs the wire to constrain from racking, in this case.
- *really* pinching and tensioning the wire is kritical,
- the PIP design would benefit from having a kind of universally-swappable drive-unit getdown, so that this can be updated later w/o changing elsewise: i.e. for gear reductions, etc...
- totally, the strength of this design rests on good cable implementations. recall that tension is big ups, and has load paths through compression... idlers, etc, all need to be stiff as well.
- concentricity: use v groove bearings native component? or two bearings per, find the crease...
- the m3 / flat head thing is OK, but there is some creak and give, if things are not super tight. either preload more, or tighten to some spec? 5mm id bearing, some 3dp / milled-in shaft?
- there is just a small chamfer missing on the bed / carriage interface (i.e. where the bed-carriage 3dp meets the bed layup)
- having hard-stops at-perpendicularness is good for laying out / squaring up on wires.   
- another point for FHCS-not-appropriate-way-to-pin-concentricity on these bearings: either do M4 & M4 Inserts, or go again down to 623ZZ ... *and* since you really want to cinch these, *and* there will be mucho vibration over time, make pockets for nylong locknuts. thx.

![smf-machine](2020-01-28_smf-machining-zund.mp4)
![smf-assy](2020-01-29_smf-assy-01.jpg)
![smf-assy](2020-01-29_smf-assy-02.jpg)
![smf-assy](2020-01-29_smf-assy-03.jpg)
![smf-assy](2020-01-29_smf-unit-mistake.jpg)
![smf-flex](2020-01-29_smf-flexflex.mp4)

## 2020 01 26

OK, closing in on end-of-fab (just fab, not assembly) here. And I am thinking that this is (we hope) a good first pass on the way to the LC-PIP model: the ubiquitous, dirty simple version of the same, for circuit milling and 3DP. In that case, the 'pinch' in the z-axis is gone, so you're left with big energy to go tall, throw the motors underneath the bed, and otherwise make the working footprint ~ only slightly smaller than the machine footprint. Is it corexy? Only maybe.

This thing:
X: 413 mm / 16.25"
Y: 232 mm / 9.1"
Z: 63 mm / 2.5"

It's kind of not impressive in any dimension haha ... so maybe we are just in prototype land, and the first PIP will just be a controller retrofit on all of this wiring etc. I figure PIP then can go 250x250x250mm, roughly. I think with more leniency in the z-axis, I can really tuck a lot of the sideways extra-space in...

I wanted to note also a drawing from the cable routing results, check it out:

![img](2020-01-26_routes-alltogether.jpg)
![img](2020-01-26_routes-z.jpg)
![img](2020-01-26_routes-y.jpg)

## 2020 01 24

I'd be interested in having one of these together on Monday, which feels like something of a stretch, but maybe not? Last rights:

- psu / power entry mount, router mount, hello cable world... or grid

I'm going to name this thing... but not yet, for now: Laid Up Gantry / Capstan Drive Small Friend: LC-SMF

I think I'm through the details, that was roughly 1.5 days... now I'm just double checking for any other missed subtractions. Got some of those... then it's PSU and router mounting, some cable notches... oh, and I would like some mounting holes for the jaw-brace toolchanger thing. I'll actually leave the PSU floating for now, on one of those tripods... and I'll just drop in a 20mm grid on the interior side, for routers and whatnot.

## 2020 01 23

OK: bless up, just cinched the routing for YZ. A true whirlwind experience. Last rights are to button down the YZ motors (I think I can use the same part for both: I ordered a planetary-reduced Z drop-in, in case I need this). Past that, and then touching the bed up some (bringing z-clearances back under the structure), I should run a cable routing sanity check in Rhino.

OK: and the bed looks good. I think I'm out tonight, then. Want to rename / shuffle some things, separate this from the 'PGD LUG' project... this is... the Steely Glint? Separate by pulling PGD LUG export -> new project. Before you go forwards... last chance to look at a smaller version. Though I am really p sure that

## 2020 01 21

wowee, time goes and designs do not complete. here I am

this morning, first order is making bed-blister ah part, using it on yr yl and x-grips, and tee-ing out the face of the bed. Oh, and recall storage condition: bed inside. what does this mean for the other side of the bed: where does that last pulley touch off?

Yeah, ok, having a tough time closing this out... because, like, what do I even want, man? Mostly the packing it up is a real PITA.

OK, yeah, I think what I really yearn for is a tightly bounding-boxed machine, so I want to move these motor units inboard of the frame, into the bed-dead-zone. I'm pretty sure real due dilligence is also putting a reduction on the z-stage. And those motor-ends should face up, to access electronics... aaand there should be some space beneath them to route cables etc.

The box is 680x400x190 interior dimensions. I am privy to make a machine ~ 400x400x190, in that case. JK: staying at 600 because the ~ 80mm depth at small bed size seems to make a lame machine. Do variant for lab takeover circuit mill / 3DP. That work: machines have 1/16" endmill and engraving endmill, in two spindles, and rapid swap them. Alternately, ++, they have 3DP heads. Same bed? Maybe. Hotswap beds more likely. Very cool would be 3DP machine w/ 5ax mill to cleanup mech. surfaces, yes. Demonstrating this is possible, compelling.

The things to do:
- side-flange skinny adjust-up-down device, for left/right bed ears
- bed structure: the T, ears to formerly mentioned devices, tail to tail (overlap) 5-beam structure as in notebook,
- with no more dependence on beam-type thing (historic) to support the motor, this wants to become symmetric. do so by re-upping the STLUG CAD to not-have-joinery to start, now you can reconsider the bed-in-x rail: is the front face the most appropriate place for this? or should you cut a small slider in behind it, to reach through?

OK, I think I have the outline of this thing now. Left is... setting up all of the pulleys (yikes, Y - then Z?) again... after the bed-adjust thing...

Wow - it's a monster. Back through the z, last rights is clearance at the rear tee-thing for the device that is going to grip / tension the cable back here.

## 2020 01 16

OK: currently annealing through carriage, cassette, etc, for new sizes. This 3/16" McMaster phenolic is really 5mm thick.

Through the PGD version of this thing, I think. Deciding whether or not it's worth the time / complexity to close the beam out on the backside. My main beef is that this is going to make it trickier to parameterize the motor placement, and to string the rope through the back face, which is already going to be a pain. Easier to add it later / less work now. I should try to start working in on the Y/Z box. Not feeling especially sharp today, I wonder what's up... Maybe a bit nervous about el bro showing up.

I wonder if the move is to work in a Y system first, or the Z.

I am really wanting like vr cad / better sketching tools here, for the capstan routing on z/y axis.

OK: I am trying to make a move that I don't like just to squeeze z-space into this travelling machine. I think that's a bit silly, I should just make a variant for taller operations, for the having-lots-at-the-cba version, which will (I figure) be a smaller bed size anyways. Or there's a 'tall and skinny' and a 'short and wide' version, whatever. This is the short and wide, ok. More generalist. Sling it low, bb.

OK, through YR, now YL wants a pinch-but-not-cut wire-hold... I should be able to use the same device for the three z-points as well. Got that... now the bed is it, maybe I can throw hella prints on tonight.

## 2020 01 13

Zurich Airport here, checking in from purgu2ory. As it happens they are actually playing some semi-hip kinda-neat electro-jazz that *isnt* airport muzak. Go swiss.

- want webs in this beam,
- let's put the wire right dead center on beams, and locate it out-of-plane wise by midsection from the same.

About that bending radius. The spec from McMaster for the size-up was ~ 8* the diameter. Here I am wishing that I could post in to this page a tiny spreadsheet... hmmm... alas,

> This means that my bending diameter should be 27mm minimum for the rope I am using: 0.047"

This is some confusing language from my notes above. I know that 27mm diameter *did* feel good (and was to spec) for that wire rope which *was* indeed 1.6mm in diameter. So, bending *radius* (13.5mm / 1.6mm) is - yep, ok - 8.5. Wham. So: now I am thinking I will step down to this 0.047" diameter, actually, which is available from a slightly different supplier on mcmaster, and only rated to 40lb (?) - 1.2mm ... 20mm OD items.

Now: this is ~ 1.6x the size of ahn GT2 20t pulley, so we drop resolution / torque by as much. There's always microstepping, so I wouldn't worry about that *too* much, but I might scratch my head about torque. *however* I recall that previous spreadsheets (offline now: pls use excel) showed a real lack of want-for-torque, at least to accel. This is mostly making me itch because of the z / bed axis.

I think the answer then is to upcity to a tallcan motor for the y-axis (bc gantry weight), probably for the bed as well, and then also throw a reduction on *just* the bed: but do this in any case in a way where the transmission units can be swapped- for reduced / not-reduced types.

## 2020 01 12

Looking at bearings to downsize gantries to now, for some smallfriends.

trade no. | id | od | t | dynamic (kN) | static (kN) | p (1, vxb)
--- | --- | --- | --- | --- | --- | ---
625 | 5 | 16 | 5 | 1.7 | 0.67 | 2.4
605 | 5 | 14 | 5 | 1.3 | 0.51 | 5.1
624 | 4 | 13 | 5 | 1.3 | 0.5 | 3.95 (pick)
604 | 4 | 12 | 4 | 1 | 0.35 | 4.7
619/4 | 4 | 11 | 4 | 1 | 0.35 | n/avail
623 | 3 | 10 | 4 | 0.63 | 0.22 | 6.7

Want to order, to have ready:
624z, 3/16" sheet to measure, cable

Also key for simplicity is cable that can git-wound w/o a drivetrain reduction... or do we bite it and do the reduction? Maybe some design could easily accommodate either/or.

Then design sequence is: new gantry 1-off, d & b.
- new transmission block: 2 reduce or not 2 reduce, and please some cable routing craft
- if you do reduce, stepper betwixt webs back-out, none of this cutout mess?
- ntb: what is bend radius of wire? ctrl+f / email mcmaster reps
- downsizing: to 10mm / 10mm SHCS / FHCS std. issues? to lock-nut inserts? assume folks don't have mcmaster for the basics ...
- cable tensioning / hold-on device: use flexure-to-pinch, eliminating some height

Then a machine: as previously, bed dropping, ample space for end-effectors ... ~ 80mm across, 120 tall?, use wire whip for small-space y 'idler' side, and for bed... should be par for course?

## 2020 01 04

Sort flat stacks for the whole machine. Some nice things:
- x axis unscrews from y-axis rollers, slides (forwards) off
- x/y axis/carriage assy should slide fully off of the front of the machine, make space for this.
- both of these work nicely when carriages can slide over idler mounts / with the cassette undone, ok
- then bed drops in with xy off,
- should be possible to drop bed all the way to the floor, with the capstan whips. how nice.
- y axis: consider that side-to-side parallel-ness will be tough: consider just one side pins lateral stiffness? two bearings up for load distro, pinched together? nice kinematic lesson. make only this one side beam-ish, the other is then shorter, just holds rollers, and up-down constraint.
- two cassettes on the y-axis: want to be able to tune easily to one another, to match.

W/R/T to the capstan (pls drop these notes in capstan project), the winding-around-two-spools thing seems to work, but is an absolute pain to thread. This could be abetted with some well-done design / guides... ideally during assembly a clean-cut wire is just pushed into a hole on one side, emerges from one on the other, fully wound...

I am thinking also that perhaps the move is for a smaller bearing, smaller wire, smaller reduction, 3/16" or similar material, and then designs more on the 'cute' side, really down to 3D printing scale, etc. This is turning into your desire for a larger format milling machine which should just be a big device. Your travel size, and the proliferating thing, should really be desktop-ish. Impressive / exciting things are like: camera module integrations, probing, using 5-ax milling to bore holes in 3D printed parts, or insert things into them, pnp can be this small anyways, multi-3dp (varied nozzle size, filaments, etc etc), etc. Here, the machine probably *is* a bed-dropping thing. Oy!

More thoughts on this: small bearings, down to 4mm / 3mm bore: and M3 FHCS to pinch against surface w/ small nipple / shoulder. Flat Head does the centering, is low profile cap. Pinchy Pinchy.

## 2020 01 01

Apres-la moment of truth w/ the windings, I got a wrap on this together. Here's the cassette. I think I can still walk this in: the lower side should be a tiny flexure, the top does tension. OK.

![cassette](2020-01-01_cap-cassette.jpg)

## 2019 12 29

I want to see the capstan equation, and in a spreadsheet. Then work out my diameter, n_turns, wire diameter, what's that angle at the capstan, how does it affect transmission ratio, spreadsheets.com, reduction (or none?) ... make the unit, the pincher, a beam, a gantry ... ok

So, I looked at the bend radius for this wire rope: McMaster lists one of the sizes' recommended sheave diameter, and I calculated a rope diameter : bend radius ratio (about 8.5) for this type of wire rope. This means that my bending diameter should be 27mm minimum for the rope I am using: 0.047" diameter wire core, 1/16" OD with coating. That's 8930T32 . I think I am going to actually use this, though my earlier 'prototype' capstan axis had much smaller bending radii: I noticed those (after the cable had been sitting some time) put some memory into the wire rope of where it had been on the rollers/pulleys. If the vision for these machines is to turn them into workhorses, lets respect the parameters. I'll also put this into the idlers, which are currently running around 17mm od. This should introduce large-ish changes to the design, hopefully I can take those all up tonight, to print a test.

Last rights on details also includes detailing joinery on the carriage: shouldn't be too long. I'll also add some stray mounts to it...

Here's an airport sketch that pretty much sums up this machine, or what the lug/capstan design primitives will wrap into... (dropping bed, whipped and light y-axis...)
![sketch](2019-12-29_lug-machine.jpg)
