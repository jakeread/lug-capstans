# LC-SMF

## Laid Up Gantries, Capstan Drives, Small Machine Format

Concurrent works are often non-separable.

Herein contains build logs / notes / etc for gantry systems or machines designed w/ laid up composite structures (practice: phenolic on optical tables) in mind, as well as the capstan drive / transmissions.

![smf-machining](log/2020-01-28_smf-machining-zund.mp4)

### PN Table

| what | pn | etc |
| --- | --- | --- |
| Wire Rope | 8930T54 | |
| Loop Compression | 3897T31 |(100% rope capacity) |
| Stop Compression | 3926T42 | (40% rope capacity) |

### Bearing Table

trade no. | id | od | t | dynamic (kN) | static (kN) | p (1, vxb)
--- | --- | --- | --- | --- | --- | ---
625 | 5 | 16 | 5 | 1.7 | 0.67 | 2.4
605 | 5 | 14 | 5 | 1.3 | 0.51 | 5.1
**624** | 4 | 13 | 5 | 1.3 | 0.5 | 3.95 (pick)
604 | 4 | 12 | 4 | 1 | 0.35 | 4.7
619/4 | 4 | 11 | 4 | 1 | 0.35 | n/avail
623 | 3 | 10 | 4 | 0.63 | 0.22 | 6.7

## Design Takeaways

Concentricity on idlers is important: use mfg'd v-groove bearings or lathe parts to do this, otherwise tension will not be constant w/ idler-roller-phase changes. Tensioning should be done *properly* as in with crimp-on wire rope fittings, not by trying to pinch it with a screw head. The cost of these parts is low and the tool is only ~ $50. Remember that tension has a return path through the chassis, and this should be considered as part of drive train stiffness (this is always true of belts as well). Recall that idlers form an important part of this chain, as they link the tension -> the compression, and they should be appropriately stiff, as well. Avoid cantilevered idlers where you can. Bending radii are nice to respect. Consider the individual who will route this wire through your machine (is it you?) leave holes for fingers. Recall the wind and thousand petals. Time, heaving like a knife. For multiple 'pick-ups' on one line, cut the wire and crimp again: aligning multiple capstan-wrap hold-ons is a PITA.

## Capstans

Everyone loves cable transmissions, and I have been feeling the capstan desire for a while. Having finished my thesis on control nets, I yearn for some machine designs. Ok.

My last few days were looking at how-to-do-this-successfully. I had a failed start first during machine week, where I made a tiny capstan axis that was wickedly simple and had within it all of the kind of naive moves we can expect. I will not bore us with the details: this is about 'cable walk' on idlers and pulleys. Namely: given some cable and some capstan, we cannot change the relationship between that cable, its position on the capstan, and the capstan's position along that cable's length, without shearing the cable 'along' the capstan. That is, if we consider the point-along-the-capstan where the cable first made contact, we cannot move that point of contact as the cable makes its way around the turn. This is OK for rack-and-pinion type capstans, but not cool when we (like we do) want to have oodles of cable run with only a small capstan. At first I tried this with just flanges: no good, with a flange (or any taper) we slightly modify the capstan's transmission ratio as the cable-to-capstan contact point in/de-creases in diameter. I then tried to use two tiny idlers just-adjacent-the-capstan to coerce the wire to shear along the surface of the capstan. I figured the thing would always try to find the absolute shortest path between the two idlers, but of course the friction of the cable on the capstan (the whole capstan value-proposition) undoes this: the thing walks. So, to the solution:

![cap-working](log/2020-01-04_capstan_success.mp4)
![cap-down](log/2020-01-04_cap-topdown.jpg)
![cap-down](log/2020-01-04_cap-across.jpg)

I am sure someone else has done this somewhere, but the move is to take one wrap on the capstan, then do the lateral traversal in free space, and wrap around another idler, then bring it back. Grooves in capstan/idler make tracking obvious.

This is cool and seems to work, the trouble is in the setup: wrapping these is a PITA of the first order, especially since this transmission is often tucked inside of a beam or similar. To aid, I think some fancy CAD can build a pattern of guides where we just push one end of the rope in and have it emerge from the other side.

Also: I made a spreadsheet for capstan equations. TBD is how to include that here: should just be ahn .xlsx, but cooler would be some JS scratch w/ string fn evaluations. As a result of some large bending radii of these cables, I am implementing a reduction betwen the motor -> the capstan drive.

So, I looked at the bend radius for this wire rope: McMaster lists one of the sizes' recommended sheave diameter, and I calculated a rope diameter : bend radius ratio (about 8.5) for this type of wire rope. This means that my bending diameter should be 27mm minimum for the rope I am using: 0.047" diameter wire core, 1/16" OD with coating. That's 8930T32 .

## Gantries

I have similarly been excited about epoxy gantries for a while: the idea is to take up some super-straightness from a reference surface (for me: the stray optical tables in 023, for most: granite) by laying up a structure on that surface. This is a way to use cheap materials in high performance machines, and the 'tooling' is very generalized.

![lug](log/2020-01-04_lug-face.jpg)

For this rail, that constrains 'planar' motion of the gantry, I image some existing straight thing:

![lug](log/2020-01-04_lug-rail.jpg)

The gantries feel stiff / light / etc.

![lug](log/2020-01-04_lug.jpg)

I've also moved on to milling phenolic on the zund, which (besides the epoxy) does a lot for stiffness. There's no rules on materials here: the same design could also be garolite (G10 / FR1), acrylic w/ acrylic cement, carbon fiber, even steel (although at this point the epoxy strength would likely be order-of-magnitude less than the material strength, check?).

## Sizes

I am thinking also that perhaps the move is for a smaller bearing, smaller wire, smaller reduction, 3/16" or similar material, and then designs more on the 'cute' side, really down to 3D printing scale, etc. This is turning into your desire for a larger format milling machine which should just be a big device. Your travel size, and the proliferating thing, should really be desktop-ish. Impressive / exciting things are like: camera module integrations, probing, using 5-ax milling to bore holes in 3D printed parts, or insert things into them, pnp can be this small anyways, multi-3dp (varied nozzle size, filaments, etc etc), etc. Here, the machine probably *is* a bed-dropping thing. Oy!

More thoughts on this: small bearings, down to 4mm / 3mm bore: and M3 FHCS to pinch against surface w/ small nipple / shoulder. Flat Head does the centering, is low profile cap. Pinchy Pinchy. Maybe even 3mm / 1/8" sheet stock?

## Never Forget

- The box has 680x400x190 interior dimensions.
- the bed needs more than those three vertical rollers: one in either x-y direction, and / to lock rotation. !
- check clearance for wire routing, esp. y right-side at the moment
