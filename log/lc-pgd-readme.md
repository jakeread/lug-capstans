# Laid Up Gantries

Concurrent works are often non-separable.

Herein contains build logs / notes / etc for gantry systems or machines designed w/ laid up composite structures (practice: phenolic on optical tables) in mind, as well as the capstan drive / transmissions.

## Capstans

Everyone loves cable transmissions, and I have been feeling the capstan desire for a while. Having finished my thesis on control nets, I yearn for some machine designs. Ok.

My last few days were looking at how-to-do-this-successfully. I had a failed start first during machine week, where I made a tiny capstan axis that was wickedly simple and had within it all of the kind of naive moves we can expect. I will not bore us with the details: this is about 'cable walk' on idlers and pulleys. Namely: given some cable and some capstan, we cannot change the relationship between that cable, its position on the capstan, and the capstan's position along that cable's length, without shearing the cable 'along' the capstan. That is, if we consider the point-along-the-capstan where the cable first made contact, we cannot move that point of contact as the cable makes its way around the turn. This is OK for rack-and-pinion type capstans, but not cool when we (like we do) want to have oodles of cable run with only a small capstan. At first I tried this with just flanges: no good, with a flange (or any taper) we slightly modify the capstan's transmission ratio as the cable-to-capstan contact point in/de-creases in diameter. I then tried to use two tiny idlers just-adjacent-the-capstan to coerce the wire to shear along the surface of the capstan. I figured the thing would always try to find the absolute shortest path between the two idlers, but of course the friction of the cable on the capstan (the whole capstan value-proposition) undoes this: the thing walks. So, to the solution:

![cap-working](log/2020-01-04_capstan_success.mp4)
![cap-down](log/2020-01-04_cap-topdown.jpg)
![cap-down](log/2020-01-04_cap-across.jpg)

I am sure someone else has done this somewhere, but the move is to take one wrap on the capstan, then do the lateral traversal in free space, and wrap around another idler, then bring it back. Grooves in capstan/idler make tracking obvious.

This is cool and seems to work, the trouble is in the setup: wrapping these is a PITA of the first order, especially since this transmission is often tucked inside of a beam or similar. To aid, I think some fancy CAD can build a pattern of guides where we just push one end of the rope in and have it emerge from the other side.

Also: I made a spreadsheet for capstan equations. TBD is how to include that here: should just be ahn .xlsx, but cooler would be some JS scratch w/ string fn evaluations. As a result of some large bending radii of these cables, I am implementing a reduction betwen the motor -> the capstan drive.

So, I looked at the bend radius for this wire rope: McMaster lists one of the sizes' recommended sheave diameter, and I calculated a rope diameter : bend radius ratio (about 8.5) for this type of wire rope. This means that my bending diameter should be 27mm minimum for the rope I am using: 0.047" diameter wire core, 1/16" OD with coating. That's 8930T32 .

## Gantries

I have similarly been excited about epoxy gantries for a while: the idea is to take up some super-straightness from a reference surface (for me: the stray optical tables in 023, for most: granite) by laying up a structure on that surface. This is a way to use cheap materials in high performance machines, and the 'tooling' is very generalized.

![lug](log/2020-01-04_lug-face.jpg)

For this rail, that constrains 'planar' motion of the gantry, I image some existing straight thing:

![lug](log/2020-01-04_lug-rail.jpg)

The gantries feel stiff / light / etc.

![lug](log/2020-01-04_lug.jpg)

I've also moved on to milling phenolic on the zund, which (besides the epoxy) does a lot for stiffness. There's no rules on materials here: the same design could also be garolite (G10 / FR1), acrylic w/ acrylic cement, carbon fiber, even steel (although at this point the epoxy strength would likely be order-of-magnitude less than the material strength, check?).

## Sizes

I am thinking also that perhaps the move is for a smaller bearing, smaller wire, smaller reduction, 3/16" or similar material, and then designs more on the 'cute' side, really down to 3D printing scale, etc. This is turning into your desire for a larger format milling machine which should just be a big device. Your travel size, and the proliferating thing, should really be desktop-ish. Impressive / exciting things are like: camera module integrations, probing, using 5-ax milling to bore holes in 3D printed parts, or insert things into them, pnp can be this small anyways, multi-3dp (varied nozzle size, filaments, etc etc), etc. Here, the machine probably *is* a bed-dropping thing. Oy!

More thoughts on this: small bearings, down to 4mm / 3mm bore: and M3 FHCS to pinch against surface w/ small nipple / shoulder. Flat Head does the centering, is low profile cap. Pinchy Pinchy. Maybe even 3mm / 1/8" sheet stock?

# Log

## 2019 12 29

I want to see the capstan equation, and in a spreadsheet. Then work out my diameter, n_turns, wire diameter, what's that angle at the capstan, how does it affect transmission ratio, spreadsheets.com, reduction (or none?) ... make the unit, the pincher, a beam, a gantry ... ok

So, I looked at the bend radius for this wire rope: McMaster lists one of the sizes' recommended sheave diameter, and I calculated a rope diameter : bend radius ratio (about 8.5) for this type of wire rope. This means that my bending diameter should be 27mm minimum for the rope I am using: 0.047" diameter wire core, 1/16" OD with coating. That's 8930T32 . I think I am going to actually use this, though my earlier 'prototype' capstan axis had much smaller bending radii: I noticed those (after the cable had been sitting some time) put some memory into the wire rope of where it had been on the rollers/pulleys. If the vision for these machines is to turn them into workhorses, lets respect the parameters. I'll also put this into the idlers, which are currently running around 17mm od. This should introduce large-ish changes to the design, hopefully I can take those all up tonight, to print a test.

Last rights on details also includes detailing joinery on the carriage: shouldn't be too long. I'll also add some stray mounts to it...

Here's an airport sketch that pretty much sums up this machine, or what the lug/capstan design primitives will wrap into... (dropping bed, whipped and light y-axis...)
![sketch](log/2019-12-29_lug-machine.jpg)

## 2020 01 04

Sort flat stacks for the whole machine. Some nice things:
- x axis unscrews from y-axis rollers, slides (forwards) off
- x/y axis/carriage assy should slide fully off of the front of the machine, make space for this.
- both of these work nicely when carriages can slide over idler mounts / with the cassette undone, ok
- then bed drops in with xy off,
- should be possible to drop bed all the way to the floor, with the capstan whips. how nice.
- y axis: consider that side-to-side parallel-ness will be tough: consider just one side pins lateral stiffness? two bearings up for load distro, pinched together? nice kinematic lesson. make only this one side beam-ish, the other is then shorter, just holds rollers, and up-down constraint.
- two cassettes on the y-axis: want to be able to tune easily to one another, to match.

## 2020 01 01

Apres-la moment of truth w/ the windings, I got a wrap on this together. Here's the cassette. I think I can still walk this in: the lower side should be a tiny flexure, the top does tension. OK.

![cassette](log/2020-01-01_cap-cassette.jpg)

## 2020 01 04

W/R/T to the capstan (pls drop these notes in capstan project), the winding-around-two-spools thing seems to work, but is an absolute pain to thread. This could be abetted with some well-done design / guides... ideally during assembly a clean-cut wire is just pushed into a hole on one side, emerges from one on the other, fully wound...

I am thinking also that perhaps the move is for a smaller bearing, smaller wire, smaller reduction, 3/16" or similar material, and then designs more on the 'cute' side, really down to 3D printing scale, etc. This is turning into your desire for a larger format milling machine which should just be a big device. Your travel size, and the proliferating thing, should really be desktop-ish. Impressive / exciting things are like: camera module integrations, probing, using 5-ax milling to bore holes in 3D printed parts, or insert things into them, pnp can be this small anyways, multi-3dp (varied nozzle size, filaments, etc etc), etc. Here, the machine probably *is* a bed-dropping thing. Oy!

More thoughts on this: small bearings, down to 4mm / 3mm bore: and M3 FHCS to pinch against surface w/ small nipple / shoulder. Flat Head does the centering, is low profile cap. Pinchy Pinchy.

## 2020 01 12

Looking at bearings to downsize gantries to now, for some smallfriends.

trade no. | id | od | t | dynamic (kN) | static (kN) | p (1, vxb)
--- | --- | --- | --- | --- | --- | ---
625 | 5 | 16 | 5 | 1.7 | 0.67 | 2.4
605 | 5 | 14 | 5 | 1.3 | 0.51 | 5.1
624 | 4 | 13 | 5 | 1.3 | 0.5 | 3.95 (pick)
604 | 4 | 12 | 4 | 1 | 0.35 | 4.7
619/4 | 4 | 11 | 4 | 1 | 0.35 | n/avail
623 | 3 | 10 | 4 | 0.63 | 0.22 | 6.7

Want to order, to have ready:
624z, 3/16" sheet to measure, cable

Also key for simplicity is cable that can git-wound w/o a drivetrain reduction... or do we bite it and do the reduction? Maybe some design could easily accommodate either/or.

Then design sequence is: new gantry 1-off, d & b.
- new transmission block: 2 reduce or not 2 reduce, and please some cable routing craft
- if you do reduce, stepper betwixt webs back-out, none of this cutout mess?
- ntb: what is bend radius of wire? ctrl+f / email mcmaster reps
- downsizing: to 10mm / 10mm SHCS / FHCS std. issues? to lock-nut inserts? assume folks don't have mcmaster for the basics ...
- cable tensioning / hold-on device: use flexure-to-pinch, eliminating some height

Then a machine: as previously, bed dropping, ample space for end-effectors ... ~ 80mm across, 120 tall?, use wire whip for small-space y 'idler' side, and for bed... should be par for course?

## 2020 01 13

Zurich Airport here, checking in from purgu2ory. As it happens they are actually playing some semi-hip kinda-neat electro-jazz that *isnt* airport muzak. Go swiss.

- want webs in this beam,
- let's put the wire right dead center on beams, and locate it out-of-plane wise by midsection from the same.

About that bending radius. The spec from McMaster for the size-up was ~ 8* the diameter. Here I am wishing that I could post in to this page a tiny spreadsheet... hmmm... alas,

> This means that my bending diameter should be 27mm minimum for the rope I am using: 0.047"

This is some confusing language from my notes above. I know that 27mm diameter *did* feel good (and was to spec) for that wire rope which *was* indeed 1.6mm in diameter. So, bending *radius* (13.5mm / 1.6mm) is - yep, ok - 8.5. Wham. So: now I am thinking I will step down to this 0.047" diameter, actually, which is available from a slightly different supplier on mcmaster, and only rated to 40lb (?) - 1.2mm ... 20mm OD items.

Now: this is ~ 1.6x the size of ahn GT2 20t pulley, so we drop resolution / torque by as much. There's always microstepping, so I wouldn't worry about that *too* much, but I might scratch my head about torque. *however* I recall that previous spreadsheets (offline now: pls use excel) showed a real lack of want-for-torque, at least to accel. This is mostly making me itch because of the z / bed axis.

I think the answer then is to upcity to a tallcan motor for the y-axis (bc gantry weight), probably for the bed as well, and then also throw a reduction on *just* the bed: but do this in any case in a way where the transmission units can be swapped- for reduced / not-reduced types.

## 2020 01 16

OK: currently annealing through carriage, cassette, etc, for new sizes. This 3/16" McMaster phenolic is really 5mm thick.

Through the PGD version of this thing, I think. Deciding whether or not it's worth the time / complexity to close the beam out on the backside. My main beef is that this is going to make it trickier to parameterize the motor placement, and to string the rope through the back face, which is already going to be a pain. Easier to add it later / less work now. I should try to start working in on the Y/Z box. Not feeling especially sharp today, I wonder what's up... Maybe a bit nervous about el bro showing up.

I wonder if the move is to work in a Y system first, or the Z.

I am really wanting like vr cad / better sketching tools here, for the capstan routing on z/y axis.

OK: I am trying to make a move that I don't like just to squeeze z-space into this travelling machine. I think that's a bit silly, I should just make a variant for taller operations, for the having-lots-at-the-cba version, which will (I figure) be a smaller bed size anyways. Or there's a 'tall and skinny' and a 'short and wide' version, whatever. This is the short and wide, ok. More generalist. Sling it low, bb.

OK, through YR, now YL wants a pinch-but-not-cut wire-hold... I should be able to use the same device for the three z-points as well. Got that... now the bed is it, maybe I can throw hella prints on tonight.

## 2020 01 21

wowee, time goes and designs do not complete. here I am

this morning, first order is making bed-blister ah part, using it on yr yl and x-grips, and tee-ing out the face of the bed. Oh, and recall storage condition: bed inside. what does this mean for the other side of the bed: where does that last pulley touch off?

Yeah, ok, having a tough time closing this out... because, like, what do I even want, man? Mostly the packing it up is a real PITA.

OK, yeah, I think what I really yearn for is a tightly bounding-boxed machine, so I want to move these motor units inboard of the frame, into the bed-dead-zone. I'm pretty sure real due dilligence is also putting a reduction on the z-stage. And those motor-ends should face up, to access electronics... aaand there should be some space beneath them to route cables etc.

The box is 680x400x190 interior dimensions. I am privy to make a machine ~ 400x400x190, in that case. JK: staying at 600 because the ~ 80mm depth at small bed size seems to make a lame machine. Do variant for lab takeover circuit mill / 3DP. That work: machines have 1/16" endmill and engraving endmill, in two spindles, and rapid swap them. Alternately, ++, they have 3DP heads. Same bed? Maybe. Hotswap beds more likely. Very cool would be 3DP machine w/ 5ax mill to cleanup mech. surfaces, yes. Demonstrating this is possible, compelling.

The things to do:
- side-flange skinny adjust-up-down device, for left/right bed ears
- bed structure: the T, ears to formerly mentioned devices, tail to tail (overlap) 5-beam structure as in notebook,
- with no more dependence on beam-type thing (historic) to support the motor, this wants to become symmetric. do so by re-upping the STLUG CAD to not-have-joinery to start, now you can reconsider the bed-in-x rail: is the front face the most appropriate place for this? or should you cut a small slider in behind it, to reach through?

OK, I think I have the outline of this thing now. Left is... setting up all of the pulleys (yikes, Y - then Z?) again... after the bed-adjust thing...

## Never Forget

- the bed needs more than those three vertical rollers: one in either x-y direction, and / to lock rotation. !
- check clearance for wire routing, esp. y right-side at the moment
